//COMPLETED
#ifndef _DATE_H
#define _DATE_H
#include <time.h>

typedef struct 
{
    int day;
    int month;
    int year;
}date;

void accept_date(date *d);
void print_date(date *d);
date add_days(date d, int days);
int days_check(int month, int year);
int leap_check(int year);
int date_cmp(date d1, date d2);
int date_equal(date d1, date d2);
int date_greater(date d1, date d2);
date date_current();

#endif