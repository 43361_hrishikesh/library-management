//COMPLETED
#include "library.h"
#include "date.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void accept_date(date *d)
{
    printf("Enter Date (dd/mm/yyy):");
    scanf("%d/%d/%d", &d->day, &d->month, &d->year);
}

void print_date(date *d)
{
    printf("Date: %d/%d/%d\n", d->day, d->month, d->year);
}

date add_days(date d, int days)
{
    date res = d;
    while (days > 0)
    {
        days--;
        res.day = res.day + 1;
        if (res.day > days_check(res.month, res.year))
        {
            res.day = 1;
            res.month = res.month + 1;
        }
        if (res.month > 12)
        {
            res.month = 1;
            res.year = res.year + 1;
        }
    }

    return res;
}

int days_check(int month, int year)
{
    int month_days[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    if (month == 2)
    {
        if (leap_check(year))
            month_days[2] = 29;
    }
    return month_days[month];
}

int leap_check(int year)
{
    if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
        return 1;
    return 0;
}

int date_cmp(date d1, date d2)
{
    int diff = -1, days = 0;
    date min = d1, max = d2;
    if (date_greater(d1, d2))
    {
        diff = 1;
        min = d2;
        max = d1;
    }
    while (!date_equal(min, max))
    {
        days = days + 1;
        min.day = min.day + 1;
        if (min.day > days_check(min.month, min.year))
        {
            min.day = 1;
            min.month = min.month + 1;
        }
        if (min.month > 12)
        {
            min.day = 1;
            min.month = 1;
            min.year = min.year + 1;
        }
    }
    return diff * days;
}

int date_equal(date d1, date d2)
{
    if (d1.day == d2.day && d1.month == d2.month && d1.year == d2.year)
        return 1;
    return 0;
}

int date_greater(date d1, date d2)
{
    if (d1.year > d2.year || (d1.year == d2.year && d1.month > d2.month) || (d1.year == d2.year && d1.month == d2.month && d1.day > d2.day))
        return 1;
    return 0;
}

date date_current()
{
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    date now;
    now.day = tm->tm_mday;
    now.month = tm->tm_mon + 1;
    now.year = tm->tm_year + 1900;
    return now;
}
