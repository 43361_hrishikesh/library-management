//COMPLETED
#include "library.h"
#include "date.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void librarian_area(user_t *u)
{
    int choice, mem_id;
    char name[80];
    do
    {
        printf("\n\n0. Sign Out\n1. Add member\n2. Edit Profile\n3. Change Password\n4. Add Book\n5. Find Book\n6. Edit Book\n7. Check Availability\n8. Add Copy\n9. Change Rack\n10. Issue Copy\n11. Return Copy\n12. Take Payment\n13. Payment History\n14. Display Issued Records\n15. Display all user login records\n16. Display all book names\nEnter choice: ");
        scanf("%d", &choice);
        printf("\n");
        switch (choice)
        {
        case 1: // Add member
            add_member();
            break;
        case 2: //  Edit profile
            edit_profile();
            break;
        case 3: //  Change Password
            change_password();
            break;
        case 4: //  Add Book
            add_book();
            break;
        case 5: //  Find Book
            printf("Enter book name: ");
            scanf("%s", name);
            book_find_by_name(name);
            break;
        case 6: //  Edit Book
            book_edit_by_id();
            break;
        case 7: //  Check Avaliablity
            bookcopy_checkavail_details();
            break;
        case 8: //  Add Copy
            bookcopy_add();
            break;
        case 9: //  Change Rack
            change_rack();
            break;
        case 10: //  issue book copy
            bookcopy_issue();
            break;
        case 11: // Return Copy
            bookcopy_return();
            break;
        case 12: // Take payment
            accept_fees();
            break;
        case 13: // Payment History
            payment_history(0);
            break;
        case 14: // display issued records
            display_all_issued_records();
            break;
        case 15: // Display all users
            list_all_users();
            break;
        case 16: // display all book names
            list_all_books();
            break;
        }
    } while (choice != 0);
}

void add_member()
{
    // input member details
    user_t u;
    accept_user(&u);
    // add librarian into the users file
    user_add(&u);
}

void add_book()
{
    FILE *fp;

    book_t b;
    accept_book(&b);
    b.id = get_next_book_id();

    fp = fopen(BOOK_DB, "ab");
    if (fp == NULL)
    {
        perror("cannot open books file");
        exit(1);
    }

    fwrite(&b, sizeof(book_t), 1, fp);
    printf("book added in file.\n");

    fclose(fp);
}

void book_edit_by_id()
{
    list_all_books();

    int id, found = 0;
    book_t b;

    printf("\n\nEnter book id: ");
    scanf("%d", &id);

    FILE *fptr = fopen(BOOK_DB, "rb+");
    if (fptr == NULL)
    {
        perror("cannot open books file");
        exit(1);
    }

    while (fread(&b, sizeof(book_t), 1, fptr) > 0)
    {
        if (id == b.id)
        {
            found = 1;
            break;
        }
    }

    if (found)
    {
        // take new book details from user
        long size = sizeof(book_t);
        book_t newbook;
        accept_book(&newbook);
        newbook.id = b.id;

        // take file position one record behind.
        fseek(fptr, -size, SEEK_CUR);

        // overwrite book details into the file
        fwrite(&newbook, size, 1, fptr);
        printf("book updated.\n");
    }
    else // if not found
        printf("Book not found.\n");

    fclose(fptr);
}

void bookcopy_add()
{
    bookwise_copies_report();
    bookcopy_t b;
    bookcopy_accept(&b);

    b.id = get_next_bookcopy_id();

    // add book copy into the books file
    // open book copies file.
    FILE *fptr = fopen(BOOKCOPY_DB, "ab");
    if (fptr == NULL)
    {
        perror("cannot open book copies file");
        exit(1);
    }

    // append book copy to the file.
    fwrite(&b, sizeof(bookcopy_t), 1, fptr);
    printf("book copy added in file.\n");

    fclose(fptr);
}

void change_rack()
{
    int cpy_id, found = 0;
    long int size = sizeof(bookcopy_t);
    bookcopy_t bkcpy;

    //open book copy file
    FILE *fptr = fopen(BOOKCOPY_DB, "rb+");
    if (fptr == NULL)
    {
        printf("File not found");
    }

    //accept copy id
    printf("Enter copy id: ");
    scanf("%d", &cpy_id);

    //read book copy file
    while (fread(&bkcpy, size, 1, fptr) > 0)
    {
        //if copy id is found
        if (cpy_id == bkcpy.id)
        {
            found = 1;
            printf("Current rack: %d\n", bkcpy.rack);

            printf("New rack: ");
            scanf("%d", &bkcpy.rack);

            bkcpy.id = cpy_id;
            strcpy(bkcpy.status, STATUS_AVAIL);
            break;
        }
    }

    // adjust ptr position behind
    fseek(fptr, -size, SEEK_CUR);

    //write into file
    fwrite(&bkcpy, size, 1, fptr);

    fclose(fptr);
    if (found == 0)
        printf("Copy id not found");
    else
        printf("Copy updated in new rack.\n");
}

void bookcopy_checkavail_details()
{
    int book_id, count = 0;
    bookcopy_t b;
    long int size = sizeof(bookcopy_t);

    //input book id
    printf("Enter Book id: ");
    scanf("%d", &book_id);

    // open book copies file
    FILE *fptr = fopen(BOOKCOPY_DB, "rb");
    if (fptr == NULL)
    {
        printf("File not found");
        return;
    }

    // read bookcopy records one by one
    while (fread(&b, size, 1, fptr) > 0)
    {
        // if book id is matching and status is available, print copy details
        if (b.bookid == book_id && strcmp(b.status, STATUS_AVAIL) == 0)
        {
            bookcopy_display(&b);
            count++;
        }
    }

    // close book copies file
    fclose(fptr);

    // if no copy is available, print the message.
    if (count == 0)
    {
        printf("No copy found");
    }
}

void bookcopy_issue()
{  
    {
        printf("\nList of Books:");
        list_all_books();
        printf("\n\n");
        printf("\nList of Avaliable Books Copies:\n");
        FILE *fp;
        bookcopy_t bk;
       
        fp = fopen(BOOKCOPY_DB, "rb");
        if (fp == NULL)
        {
            perror("cannot open issue record file");
            return;
        }

        while (fread(&bk, sizeof( bookcopy_t), 1, fp) > 0)
        {
            if (strcmp(bk.status,STATUS_AVAIL) == 0)
                bookcopy_display(&bk);
        }
        
        fclose(fp);
    }

    issuerecord_t rec;

    //accept issue record data
    issuerecord_accept(&rec);

    if (paid_member_check(rec.memberid) == 0)
    {
        printf("Member is not a paid user.");
        return;
    }

    //open issue record file
    FILE *fptr = fopen(ISSUERECORD_DB, "ab");
    if (fptr == NULL)
    {
        printf("file not found");
        return;
    }

    //write record into the file
    fwrite(&rec, sizeof(issuerecord_t), 1, fptr);

    fclose(fptr);

    //mark copy as issued
    bookcopy_changestatus(rec.copyid, STATUS_ISSUED);

    printf("Book with copy id : %d \"ISSUED SUCCESSFULLY\" for member id: %d\n", rec.copyid,rec.memberid);
}

void bookcopy_changestatus(int bookcopy_id, char status[])
{
    bookcopy_t b;
    long size = sizeof(bookcopy_t);

    FILE *fptr = fopen(BOOKCOPY_DB, "rb+");
    if (fptr == NULL)
    {
        perror("File not found");
        return;
    }

    while (fread(&b, sizeof(bookcopy_t), 1, fptr) > 0)
    {
        if (bookcopy_id == b.id)
        {
            strcpy(b.status, status);

            //go one record back
            fseek(fptr, -size, SEEK_CUR);

            //write updated status into book copy file
            fwrite(&b, sizeof(bookcopy_t), 1, fptr);

            break;
        }
    }
    fclose(fptr);
}

void display_issued_bookcopies(int member_id)
{
    FILE *fp;
    issuerecord_t rec;
    // open issue records file
    fp = fopen(ISSUERECORD_DB, "rb");
    if (fp == NULL)
    {
        perror("cannot open issue record file");
        return;
    }

    // read records one by one
    while (fread(&rec, sizeof(issuerecord_t), 1, fp) > 0)
    {
        // if member_id is matching and return date is 0, print it.
        if (rec.memberid == member_id && rec.return_date.day == 0)
            issuerecord_display(&rec);
    }
    // close the file
    fclose(fp);
}

void bookcopy_return()
{
    int member_id, record_id;
    FILE *fp;
    issuerecord_t rec;
    int diff_days, found = 0;
    long size = sizeof(issuerecord_t);

    // input member id
    printf("enter member id: ");
    scanf("%d", &member_id);

    // print all issued books (not returned yet)
    display_issued_bookcopies(member_id);

    // input issue record id to be returned.
    printf("enter issue record id (to return): ");
    scanf("%d", &record_id);

    // open issuerecord file
    fp = fopen(ISSUERECORD_DB, "rb+");
    if (fp == NULL)
    {
        perror("cannot open issue record file");
        return;
    }

    // read records one by one
    while (fread(&rec, sizeof(issuerecord_t), 1, fp) > 0)
    {
        // find issuerecord id
        if (record_id == rec.id && member_id == rec.memberid)
        {
            found = 1;

            // initialize return date
            rec.return_date = date_current();

            // check for the fine amount
            diff_days = date_cmp(rec.return_date, rec.return_duedate);

            // update fine amount if any
            if (diff_days > 0)
                rec.fine_amount = diff_days * FINE_PER_DAY;
            break;
        }
    }

    if (found)
    {
        // go one record back
        fseek(fp, -size, SEEK_CUR);

        // overwrite the issue record
        fwrite(&rec, sizeof(issuerecord_t), 1, fp);

        // print updated issue record.
        printf("issue record updated after returning book:\n");
        issuerecord_display(&rec);

        // update copy status to available
        bookcopy_changestatus(rec.copyid, STATUS_AVAIL);
    }
    else
    {
        printf("Enterd details not matched our records");
    }

    fclose(fp);

    if (rec.fine_amount > 0)
    {
        payment_t fin;

        //open payment file in append mode
        fp = fopen(PAYMENT_DB, "ab+");

        if (fp == NULL)
        {
            perror("cannot open payment record file");
            return;
        }

        //accepting fine details to write into payments file
        fin.id = get_next_payment_id();
        fin.memberid = member_id;
        fin.tx_time = date_current();
        fin.amount = rec.fine_amount;
        strcpy(fin.type, PAY_TYPE_FINE);
        memset(&fin.next_pay_duedate, 0, sizeof(date));

        //writing fine details into payment file
        fwrite(&fin, sizeof(payment_t), 1, fp);
        printf("Fine amount %lf updated in payments record file.", rec.fine_amount);

        fclose(fp);
    }
}

void display_all_issued_records()
{
    issuerecord_t rec;
    int choice;

    printf("\n0. To display all records\n1. To Display only returned book copy records\n2. To Display Not returned book copy records\n Enter Choice: ");
    scanf("%d", &choice);
    printf("\n\n");
    //open issue record file
    FILE *fptr = fopen(ISSUERECORD_DB, "rb");
    if (fptr == NULL)
    {
        printf("file not found");
        return;
    }

    //read record into the file
    int found = 0;
    if (choice == 0)
    {
        while (fread(&rec, sizeof(issuerecord_t), 1, fptr) > 0)
        {
            found = 1;
            issuerecord_display(&rec);
            printf("\n");
        }
    }
    else if (choice == 1)
    {
        while (fread(&rec, sizeof(issuerecord_t), 1, fptr) > 0)
        {
            if ((rec.return_date.day > 0) && (rec.return_date.month > 0) && (rec.return_date.year > 0))
            {
                found = 1;
                issuerecord_display(&rec);
                printf("\n");
            }
        }
    }
    else
    {
        while (fread(&rec, sizeof(issuerecord_t), 1, fptr) > 0)
        {
            if ((rec.return_date.day == 0) && (rec.return_date.month == 0) && (rec.return_date.year == 0))
            {
                found = 1;
                issuerecord_display(&rec);
                printf("\n");
            }
        }
    }

    if (found == 0)
    {
        printf("No records found.\n");
    }

    fclose(fptr);
}

void accept_fees()
{
    payment_t pay;

    //accept payment details
    payment_accept(&pay);

    //open payment file
    FILE *fptr = fopen(PAYMENT_DB, "ab+");
    if (fptr == NULL)
    {
        perror("unable to open payment file");
    }

    //write contents to the file
    fwrite(&pay, sizeof(payment_t), 1, fptr);

    //close the file
    fclose(fptr);

    //to display the status
    fptr = fopen(PAYMENT_DB, "rb");
    payment_t readpay;

    if (fptr == NULL)
    {
        perror("unable to open payment file");
    }

    while (fread(&readpay, sizeof(payment_t), 1, fptr))
    {
        if (pay.memberid == readpay.memberid)
        {
            payment_display(&readpay);
        }
    }

    fclose(fptr);
}

void payment_history(int member_id)
{
    FILE *fptr = fopen(PAYMENT_DB, "rb");
    payment_t readpay;

    if (member_id == -1)
    {
        goto ALLREC;
    }
    else if(member_id > 0)
    {
        goto MEMREC;
    }

    if (fptr == NULL)
    {
        perror("unable to open payment file");
    }

    int choice,found = 0;
    printf("\n1. To Display Payment History of a perticular member\n2. To Display Payment history of all members\n\nEnter choice: ");
    scanf("%d",&choice);

    switch (choice)
    {
    case 1:
        printf("Enter member id of the member: ");
		scanf("%d", &member_id);
    MEMREC:
        while(fread(&readpay, sizeof(payment_t), 1, fptr) > 0) 
        {
            found = 1;
		    if(readpay.memberid == member_id)
			payment_display(&readpay);
	    }
        break;
    
    case 2:
    ALLREC:
        while (fread(&readpay, sizeof(payment_t), 1, fptr))
        {
            found = 1;
            payment_display(&readpay);
        }
        break;
    }
    
    if (found == 0)
    {
        printf("Payment record history not avaliable.");
    }

    fclose(fptr);
}

//list all users
void list_all_users()
{
    user_t u;
    FILE *fptr = fopen(USER_DB, "rb");

    while (fread(&u, sizeof(user_t), 1, fptr) > 0)
    {
        display_user(&u);
        printf(" Password: %s\n", u.passwd);
    }
    fclose(fptr);
}

int paid_member_check(int member_id)
{
    date now = date_current();

    payment_t pay;
    int found = 0;

    FILE *fptr = fopen(PAYMENT_DB, "rb");
    if (fptr == NULL)
    {
        perror("cannot open payment file");
        return 0;
    }

    while (fread(&pay, sizeof(payment_t), 1, fptr) > 0)
    {
        if (pay.memberid == member_id && pay.next_pay_duedate.day != 0 && date_cmp(now, pay.next_pay_duedate) < 0)
        {
            found = 1;
            break;
        }
    }

    fclose(fptr);

    return found;
}

void list_all_books()
{
    book_t b;
    FILE *fptr = fopen(BOOK_DB, "rb");

    while (fread(&b, sizeof(book_t), 1, fptr) > 0)
    {
        display_book(&b);
    }
    fclose(fptr);
}
