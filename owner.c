#include "library.h"
#include "date.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void owner_area(user_t *u)
{
    int choice;
    do
    {
        printf("\n\n0. Sign Out\n1. Appoint Librarian\n2. Edit Profile\n3. Change Password\n4. Fees/Fine Report\n5. Book Availability\n6. Bookwise copies report\nEnter choice: ");
        scanf("%d", &choice);
        printf("\n\n");
        switch (choice)
        {
        case 1: // Appoint Librarian
            appoint_librarian();
            break;
        case 2: //  Edit profile
            edit_profile();
            break;
        case 3: //  Change Password
            change_password();
            break;
        case 4: //  Fees / Fine Report
            payment_history(-1);
            break;
        case 5: //book avaliablity check
            bookcopy_checkavail_details();
            break;
        case 6: //  Bookwise copies report
            bookwise_copies_report();
            break;
        }
    } while (choice != 0);
}

void appoint_librarian()
{
    user_t u;
    accept_user(&u);
    strcpy(u.role, ROLE_LIBRARIAN);
    user_add(&u);
}

void bookwise_copies_report()
{
    bookcopy_t b_copy;
    int avail = 0, issue = 0, count = 0, i;
    int n = get_next_book_id();

    FILE *fptr = fopen(BOOKCOPY_DB, "rb");
    if (fptr == NULL)
    {
        perror("File not found");
        return;
    }

    book_t b;
    FILE *fp = fopen(BOOK_DB, "rb");
    if (fp == NULL)
    {
        perror("File not found");
        return;
    }

    for (i = 1; i < n; i++)
    {
        fread(&b, sizeof(book_t), 1, fp);
        count = 0, avail = 0, issue = 0;

        while (fread(&b_copy, sizeof(bookcopy_t), 1, fptr) > 0)
        {
            if (b_copy.bookid == i)
            {
                count++;

                if (strcmp(b_copy.status, STATUS_AVAIL) == 0)
                    avail++;

                if (strcmp(b_copy.status, STATUS_ISSUED) == 0)
                    issue++;
            }
        }
        printf("\nid: %d name: %s avaliable: %d, issued: %d, count: %d\n", i, b.name, avail, issue, count);

        fseek(fptr, 0, SEEK_SET);
    }

    fclose(fp);
    fclose(fptr);
}