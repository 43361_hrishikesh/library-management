//COMPLETED
#ifndef _LIBRARY_H
#define _LIBRARY_H
#include "date.h"

#define ROLE_OWNER "owner"
#define EMAIL_OWNER "hrishi@email.com"

#define ROLE_LIBRARIAN "librarian"
#define ROLE_MEMBER "member"

#define USER_DB "users.db"
#define BOOK_DB "books.db"
#define BOOKCOPY_DB "bookcopies.db"
#define ISSUERECORD_DB "issuerecord.db"
#define PAYMENT_DB "payments.db"

#define STATUS_AVAIL "avaliable"
#define STATUS_ISSUED "issued"

#define BOOK_RETURN_DAYS 7
#define MEMBERSHIP_MONTH_DAYS 30
#define FINE_PER_DAY 5

#define PAY_TYPE_FEES "fees"
#define FEES_AMOUNT 100
#define PAY_TYPE_FINE "fine"

typedef struct user
{
    int id;
    char name[30];
    char email[30];
    char phone[15];
    char passwd[10];
    char role[15];
} user_t;

typedef struct book
{
    int id;
    char name[50];
    char author[50];
    char subject[30];
    double price;
    char isbn[13];
} book_t;

typedef struct bookcopy
{
    int id;
    int bookid;
    int rack;
    char status[16];
} bookcopy_t;

typedef struct issuerecord
{
    int id;
    int copyid;
    int memberid;
    date issue_date;
    date return_duedate;
    date return_date;
    double fine_amount;
} issuerecord_t;

typedef struct payment
{
    int id;
    int memberid;
    double amount;
    char type[10];
    date tx_time;
    date next_pay_duedate;
} payment_t;

//book functions
void accept_book(book_t *b);
void display_book(book_t *b);

//user functions
void accept_user(user_t *u);
void display_user(user_t *u);

//owner functions
void owner_area(user_t *u);
void appoint_librarian();
void subjectwise_copy_report();
void bookwise_copies_report();

//librarian functions
void librarian_area(user_t *u);
void add_member();
void add_book();
void book_edit_by_id();
void bookcopy_add();
void bookcopy_checkavail_details();
void change_rack();
void bookcopy_issue();
void bookcopy_changestatus(int bookcopy_id, char status[]);
void display_issued_bookcopies(int member_id);
void bookcopy_return();
void display_all_issued_records();
void accept_fees();
void payment_history(int member_id);
void list_all_users();
int paid_member_check(int member_id);
void list_all_books();

//member functions
void member_area(user_t *u);
int get_next_bookcopy_id();
void bookcopy_checkavail();

//common functions
void signin();
void signup();
void edit_profile();
void change_password();
void user_add(user_t *u);
int user_find_by_email(user_t *u, char email[]);
int get_next_user_id();
int get_next_book_id();
void book_find_by_name(char name[]);
int get_next_issuerecord_id();
int get_next_payment_id();

// bookcopy functions in common
void bookcopy_accept(bookcopy_t *c);
void bookcopy_display(bookcopy_t *c);

// issuerecord functions in common
void issuerecord_accept(issuerecord_t *r);
void issuerecord_display(issuerecord_t *r);

// payment functions in common
void payment_accept(payment_t *p);
void payment_display(payment_t *p);

#endif