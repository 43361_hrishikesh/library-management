//COMPLETED
#include "library.h"
#include "date.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void test()
{
    user_t u;
    accept_user(&u);
    display_user(&u);
    book_t b;
    accept_book(&b);
    display_book(&b);
}

void signin()
{
    char email[30], password[10];
    user_t u;
    int invalid_user = 0;
    printf("email: ");
    scanf("%s", email);
    printf("password: ");
    scanf("%s", password);
    if (user_find_by_email(&u, email) == 1)
    {
        if (strcmp(password, u.passwd) == 0)
        {
            if (strcmp(email, EMAIL_OWNER) == 0)
                strcpy(u.role, ROLE_OWNER);

            if (strcmp(u.role, ROLE_OWNER) == 0)
                owner_area(&u);
            else if (strcmp(u.role, ROLE_LIBRARIAN) == 0)
                librarian_area(&u);
            else if (strcmp(u.role, ROLE_MEMBER) == 0)
                member_area(&u);
            else
                invalid_user = 1;
        }
        else
            invalid_user = 1;
    }
    else
        invalid_user = 1;

    if (invalid_user)
        printf("Invalid email, password or role.\n");
}

void signup()
{
    user_t u;
    accept_user(&u);
    user_add(&u);
}

int main(void)
{
    int choice;
    do
    {
        printf("\n\n0.Exit\n1.Signin\n2.Signup\nEnter Choice: ");
        scanf("%d", &choice);
        printf("\n\n");
        switch (choice)
        {
        case 1:
            signin();
            break;
        case 2:
            signup();
            break;
        }
    } while (choice != 0);

    return 0;
}