//COMPLETED
#include "library.h"
#include "date.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void accept_user(user_t *u)
{
    //printf("id: ");
    //scanf("%d", &u->id);
    u->id = get_next_user_id();
    printf("name: ");
    scanf("%s", u->name);
    printf("email: ");
    scanf("%s", u->email);
    printf("phone: ");
    scanf("%s", u->phone);
    printf("pass: ");
    scanf("%s", u->passwd);
    strcpy(u->role, ROLE_MEMBER);
}

void display_user(user_t *u)
{
    printf("user id: %d name: %s email: %s phone: %s role: %s", u->id, u->name, u->email, u->phone, u->role);
}

void edit_profile()
{
    char email[30], pass[10];
    int found = 0;
    user_t u;

    printf("enter email: ");
    scanf("%s", email);

    printf("enter password: ");
    scanf("%s", pass);

    FILE *fptr = fopen(USER_DB, "rb+");
    if (fptr == NULL)
    {
        perror("cannot open users file");
        exit(1);
    }

    while (fread(&u, sizeof(user_t), 1, fptr) > 0)
    {
        if (strcmp(email, u.email) == 0)
        {
            if (strcmp(pass, u.passwd) == 0)
            {
                found = 1;
                break;
            }
            break;
        }
    }

    if (found)
    {
        // take new user details from user
        long size = sizeof(user_t);
        user_t newuser;

        //accept updated user details
        printf("name: ");
        scanf("%s", newuser.name);
        printf("email: ");
        scanf("%s", newuser.email);
        printf("phone: ");
        scanf("%s", newuser.phone);

        newuser.id = u.id;
        strcpy(newuser.passwd, pass);
        strcpy(newuser.role, u.role);

        // take file position one record behind.
        fseek(fptr, -size, SEEK_CUR);

        // overwrite user details into the file
        fwrite(&newuser, size, 1, fptr);
        printf("user updated.\n");
    }
    else // if not found
        printf("user not found.\n");

    fclose(fptr);
}

void change_password()
{
    char email[30], pass[10];
    int found = 0;
    user_t u;

    printf("enter email: ");
    scanf("%s", email);

    printf("enter password: ");
    scanf("%s", pass);

    FILE *fptr = fopen(USER_DB, "rb+");
    if (fptr == NULL)
    {
        perror("cannot open users file");
        exit(1);
    }

    while (fread(&u, sizeof(user_t), 1, fptr) > 0)
    {
        if (strcmp(email, u.email) == 0)
        {
            if (strcmp(pass, u.passwd) == 0)
            {
                found = 1;
                break;
            }
            break;
        }
    }

    if (found)
    {
        // take new user details from user
        long size = sizeof(user_t);
        user_t newuser;

        //accept new password
        printf("enter new password: ");
        scanf("%s", newuser.passwd);

        newuser.id = u.id;
        strcpy(newuser.name, u.name);
        strcpy(newuser.email, u.email);
        strcpy(newuser.phone, u.phone);
        strcpy(newuser.role, u.role);

        // take file position one record behind.
        fseek(fptr, -size, SEEK_CUR);

        // overwrite user details into the file
        fwrite(&newuser, size, 1, fptr);
        printf("user updated.\n");
    }
    else // if not found
        printf("user not found.\n");

    fclose(fptr);
}

void accept_book(book_t *b)
{
    //printf("id: ");
    //scanf("%d", &b->id);
    printf("name: ");
    scanf("%s", b->name);
    printf("author: ");
    scanf("%s", b->author);
    printf("subject: ");
    scanf("%s", b->subject);
    printf("price: ");
    scanf("%lf", &b->price);
    printf("isbn: ");
    scanf("%s", b->isbn);
}

void display_book(book_t *b)
{
    printf("\n Bookid: %d, %s, %s, %s, %lf, %s", b->id, b->name, b->author, b->subject, b->price, b->isbn);
}

void user_add(user_t *u)
{
    FILE *fp = fopen(USER_DB, "ab");
    if (fp == NULL)
    {
        perror("file not found");
        return;
    }
    fwrite(u, sizeof(user_t), 1, fp);
    printf("user details added into file..!!");

    fclose(fp);
}

int user_find_by_email(user_t *u, char email[])
{
    int found = 0;
    FILE *fp = fopen(USER_DB, "rb");
    if (fp == NULL)
    {
        perror("file not found");
        return found;
    }

    while (fread(u, sizeof(user_t), 1, fp) > 0)
    {
        if (strcmp(u->email, email) == 0)
        {
            found = 1;
            break;
        }
    }

    fclose(fp);

    return found;
}

// bookcopy functions
void bookcopy_accept(bookcopy_t *c)
{
    //printf("copy id: ");
    //scanf("%d", &c->id);

    printf("\nBook id: ");
    scanf("%d", &c->bookid);

    printf("Rack: ");
    scanf("%d", &c->rack);

    strcpy(c->status, STATUS_AVAIL);
}

int get_next_bookcopy_id()
{
    FILE *fptr;
    int max = 0;
    int SIZE = sizeof(bookcopy_t);
    bookcopy_t u;

    fptr = fopen(BOOKCOPY_DB, "rb");
    if (fptr == NULL)
        return max + 1;

    // change file pos to the last record
    fseek(fptr, -SIZE, SEEK_END);

    if (fread(&u, SIZE, 1, fptr) > 0)
        // if read is successful, get max (its) id
        max = u.id;

    fclose(fptr);

    return max + 1;
}

void bookcopy_display(bookcopy_t *c)
{
    printf("copy id: %d,  book id: %d,  rack: %d,  status: %s\n", c->id, c->bookid, c->rack, c->status);
}

// issuerecord functions
void issuerecord_accept(issuerecord_t *r)
{
    //printf("id: ");
    //scanf("%d", &r->id);
    r->id = get_next_issuerecord_id();
    printf("\nEnter copy id to be issued: ");
    scanf("%d", &r->copyid);
    printf("member id: ");
    scanf("%d", &r->memberid);
    printf("issue date: ");
    accept_date(&r->issue_date);
    r->return_duedate = add_days(r->issue_date, BOOK_RETURN_DAYS);
    memset(&r->return_date, 0, sizeof(date));
    r->fine_amount = 0.0;
}

int get_next_issuerecord_id()
{
    int max = 0;
    int size = sizeof(issuerecord_t);
    issuerecord_t u;
    
    FILE *fp = fopen(ISSUERECORD_DB, "rb");
    if (fp == NULL)
        return max + 1;
    
    // to change file position to the last record
    fseek(fp, -size, SEEK_END);
    
    // then to read the record from the file
    if (fread(&u, size, 1, fp) > 0)
        max = u.id; //to get max id from file
    
    fclose(fp);

    return max + 1;
}

void issuerecord_display(issuerecord_t *r)
{
    printf("issue record: %d, copy: %d, member: %d, fine: %.2lf\n", r->id, r->copyid, r->memberid, r->fine_amount);

    printf("issue ");
    print_date(&r->issue_date);

    printf("return due ");
    print_date(&r->return_duedate);

    printf("return ");
    print_date(&r->return_date);
}

// payment functions
void payment_accept(payment_t *p)
{
    //printf("id: ");
    //scanf("%d", &p->id);
    p->id = get_next_payment_id();

    printf("member id: ");
    scanf("%d", &p->memberid);

    strcpy(p->type,PAY_TYPE_FEES);

    p->amount = FEES_AMOUNT;

    p->tx_time = date_current();
    
    if (strcmp(p->type, PAY_TYPE_FEES) == 0)
        p->next_pay_duedate = add_days(p->tx_time, MEMBERSHIP_MONTH_DAYS);
    else
        memset(&p->next_pay_duedate, 0, sizeof(date));
}

//automate payment id
int get_next_payment_id()
{
    int max = 0;
    int size = sizeof(payment_t);
    payment_t u;
    
    FILE *fp = fopen(PAYMENT_DB, "rb");
    if (fp == NULL)
        return max + 1;
    
    // to change file position to the last record
    fseek(fp, -size, SEEK_END);
    
    // then to read the record from the file
    if (fread(&u, size, 1, fp) > 0)
        max = u.id; //to get max id from file
    
    fclose(fp);

    return max + 1;
}

void payment_display(payment_t *p)
{
    printf("payment: %d, member: %d, %s, amount: %.2lf\n", p->id, p->memberid, p->type, p->amount);
    printf("payment ");
    print_date(&p->tx_time);
    if ( strcmp(p->type,PAY_TYPE_FINE) == 0 )
    {
        printf("\n");
    }
    else
    {
        printf("payment due ");
        print_date(&p->next_pay_duedate);
        printf("\n");
    }
    
}

//automate user id no need to take it manually
int get_next_user_id()
{
    FILE *fp;
    int max = 0;
    int size = sizeof(user_t);
    user_t u;
    fp = fopen(USER_DB, "rb");
    if (fp == NULL)
        return max + 1;
    // to change file position to the last record
    fseek(fp, -size, SEEK_END);
    // then to read the record from the file
    if (fread(&u, size, 1, fp) > 0)
        max = u.id; //to get max id from file
    fclose(fp);

    return max + 1;
}

int get_next_book_id()
{
    FILE *fp;
    int max = 0;
    int size = sizeof(book_t);
    book_t u;

    fp = fopen(BOOK_DB, "rb");
    if (fp == NULL)
        return max + 1;

    // change file pos to the last record
    fseek(fp, -size, SEEK_END);

    // read the record from the file
    if (fread(&u, size, 1, fp) > 0)
        // if read is successful, get max (its) id
        max = u.id;

    fclose(fp);

    return max + 1;
}

void book_find_by_name(char name[])
{
    FILE *fp;
    int found = 0;
    book_t b;

    fp = fopen(BOOK_DB, "rb");
    if (fp == NULL)
    {
        perror("file not found");
        return;
    }

    // read all books one by one
    while (fread(&b, sizeof(book_t), 1, fp) > 0)
    {
        if (strstr(b.name, name) != NULL)
        {
            found = 1;
            display_book(&b);
        }
    }

    fclose(fp);

    if (!found)
        printf("Nothing Found.\n");
}
