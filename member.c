//COMPLETED
#include "library.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void member_area(user_t *u)
{
    int choice;
    char name[80];

    do
    {
        printf("\n\n\n0.Signout\n1.Find Book\n2.Edit Profile\n3.Change Password\n4.Book Avaliablity\n5.Issued Books\n6.Payment History\nEnter Choice: ");
        scanf("%d", &choice);
        printf("\n\n");
        switch (choice)
        {
        case 1:
            printf("Enter book name: ");
            scanf("%s", name);
            book_find_by_name(name);
            break;
        case 2: //  Edit profile
            edit_profile();
            break;
        case 3: //  Change Password
            change_password();
            break;
        case 4:
            bookcopy_checkavail();
            break;
        case 5:
            display_issued_bookcopies(u->id);
            break;
        case 6:
            payment_history(u->id);
            break;
        }
    } while (choice != 0);
}

void bookcopy_checkavail()
{
    int book_id, count = 0;
    bookcopy_t b;
    long int size = sizeof(bookcopy_t);

    //input book id
    printf("Enter Book id: ");
    scanf("%d", &book_id);

    // open book copies file
    FILE *fptr = fopen(BOOKCOPY_DB, "rb");
    if (fptr == NULL)
    {
        printf("File not found");
        return;
    }

    // read bookcopy records one by one
    while (fread(&b, size, 1, fptr) > 0)
    {
        // if book id is matching and status is available, count the copies
        if (b.bookid == book_id && strcmp(b.status, STATUS_AVAIL) == 0)
        {
            count++;
        }
    }

    // close book copies file
    fclose(fptr);

    // how many copies are available, print the message.
    printf("\nNo of copies found: %d\n",count);

}